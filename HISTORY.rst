.. :changelog:

History
-------

1.1.0 (2020-03-21)
++++++++++++++++++

* Change min and max django version
* New feature to add a + before the callerID in gateway for outbound calls

1.0.1 (2019-07-10)
++++++++++++++++++

* Add provider registration feature

1.0.0 (2019-05-23)
++++++++++++++++++

* Add domain to customer endpoint

0.9.3 (2019-04-10)
++++++++++++++++++

* Add P-Preferred-Identity SIP header management for provider and customer endpoint.

0.9.2 (2019-01-30)
++++++++++++++++++

* SQL views for Kamailio.
* French and spanish translations

0.9.1 (2018-12-20)
++++++++++++++++++

* Bug : typo in dependencies on PyPI.

0.9.0 (2018-12-07)
++++++++++++++++++

* First release on PyPI.
