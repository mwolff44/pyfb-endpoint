============
Installation
============

At the command line::

    $ easy_install pyfb-endpoint

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv pyfb-endpoint
    $ pip install pyfb-endpoint
