=====
Usage
=====

To use pyfb-endpoint in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'pyfb_endpoint.apps.PyfbEndpointConfig',
        ...
    )

Add pyfb-endpoint's URL patterns:

.. code-block:: python

    from pyfb_endpoint import urls as pyfb_endpoint_urls


    urlpatterns = [
        ...
        url(r'^', include(pyfb_endpoint_urls)),
        ...
    ]
