#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase
from django.urls import reverse
from django.test import Client

from pyfb_company.models import Customer, Provider, Company

from pyfb_endpoint.models import CustomerEndpoint, Codec, ProviderEndpoint


def create_company(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["address"] = "address"
    defaults["contact_name"] = "contact_name"
    defaults["contact_phone"] = "contact_phone"
    defaults["customer_balance"] = '1.5'
    defaults["supplier_balance"] = '1.5'
    defaults.update(**kwargs)
    return Company.objects.create(**defaults)


def create_customer(**kwargs):
    defaults = {}
    defaults["account_number"] = "account_number"
    defaults["credit_limit"] = '1.5'
    defaults["low_credit_alert"] = '1.5'
    defaults["max_calls"] = 1
    defaults["calls_per_second"] = 1
    defaults["customer_enabled"] = 1
    defaults.update(**kwargs)
    if "company" not in defaults:
        defaults["company"] = create_company()
    return Customer.objects.create(**defaults)


def create_provider(**kwargs):
    defaults = {}
    defaults["supplier_enabled"] = 1
    defaults.update(**kwargs)
    if "company" not in defaults:
        defaults["company"] = create_company()
    return Provider.objects.create(**defaults)


def create_customerendpoint(**kwargs):
    defaults = {}
    defaults["registration"] = 0
    defaults["password"] = "l$>,hg]IEzf7G)z8V5MDs>HC1"
    defaults["description"] = "description"
    defaults["name"] = "name"
    defaults["rtp_ip"] = "10.0.0.1/32"
    defaults["sip_ip"] = "10.0.0.1/32"
    defaults["sip_port"] = 5060
    defaults["max_calls"] = 1
    defaults["calls_per_second"] = 1
    defaults["outbound_caller_id_name"] = "outbound_caller_id_name"
    defaults["outbound_caller_id_number"] = "outbound_caller_id_number"
    defaults["force_caller_id"] = 1
    defaults["masq_caller_id"] = 1
    defaults["urgency_number"] = 1
    defaults["insee_code"] = "insee_code"
    defaults["enabled"] = 1
    defaults["fake_ring"] = 1
    defaults["cli_debug"] = 1
    defaults["transcoding_allowed"] = 1
    defaults["recording_allowed"] = 1
    defaults["recording_always"] = 1
    defaults["recording_limit"] = 10
    defaults["recording_retention"] = 10
    defaults["sip_transport"] = "udp"
    defaults["rtp_transport"] = "RTP/AVP"
    defaults["rtp_tos"] = 184
    defaults["pai"] = 1
    defaults["pid"] = 1
    defaults["ha1"] = "ha1"
    defaults["ha1b"] = "ha1b"
    defaults.update(**kwargs)
    if "customer" not in defaults:
        defaults["customer"] = create_customer()
    return CustomerEndpoint.objects.create(**defaults)


def create_codec(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["number"] = 0
    defaults["ptime"] = 20
    defaults["stereo"] = 0
    defaults["rfc_name"] = "rfc_name"
    defaults["description"] = "description"
    defaults.update(**kwargs)
    return Codec.objects.create(**defaults)


def create_providerendpoint(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["max_calls"] = 1
    defaults["enabled"] = 1
    defaults["prefix"] = "prefix"
    defaults["suffix"] = "suffix"
    defaults["username"] = "username"
    defaults["password"] = "password"
    defaults["register"] = 1
    defaults["sip_proxy"] = "sip_proxy"
    defaults["sip_transport"] = "udp"
    defaults["sip_port"] = 5060
    defaults["realm"] = "realm"
    defaults["from_domain"] = "from_domain"
    defaults["expire_seconds"] = 10
    defaults["retry_seconds"] = 10
    defaults["caller_id_in_from"] = 1
    defaults["add_plus_in_caller"] = 0
    defaults["pid"] = 1
    defaults["pai"] = 1
    defaults["calls_per_second"] = 1
    defaults["rtp_transport"] = "RTP/AVP"
    defaults["rtp_tos"] = 184
    defaults.update(**kwargs)
    if "provider" not in defaults:
        defaults["provider"] = create_provider()
    return ProviderEndpoint.objects.create(**defaults)


class CustomerEndpointViewTest(TestCase):
    '''
    Tests for CustomerEndpoint
    '''
    def setUp(self):
        self.client = Client()

    def test_list_customerendpoint(self):
        url = reverse('pyfb-endpoint:pyfb_endpoint_customerendpoint_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_customerendpoint(self):
        url = reverse('pyfb-endpoint:pyfb_endpoint_customerendpoint_create')
        data = {
            "registration": 0,
            "password": "l$>,hg]IEzf7G)z8V5MDs>HC1",
            "description": "description",
            "name": "name",
            "rtp_ip": "10.0.0.1/32",
            "sip_ip": "10.0.0.1/32",
            "sip_port": 5060,
            "max_calls": 10,
            "calls_per_second": 10,
            "outbound_caller_id_name": "outbound_caller_id_name",
            "outbound_caller_id_number": "outbound_caller_id_number",
            "force_caller_id": 1,
            "masq_caller_id": 1,
            "urgency_number": 1,
            "insee_code": "insee_code",
            "enabled": 1,
            "fake_ring": 1,
            "cli_debug": 1,
            "modified": "modified",
            "transcoding_allowed": 1,
            "recording_allowed": 1,
            "recording_always": 1,
            "recording_limit": 1,
            "recording_retention": 1,
            "sip_transport": "udp",
            "rtp_transport": "RTP/AVP",
            "rtp_tos": 184,
            "pai": 1,
            "pid": 1,
            "ha1": "ha1",
            "ha1b": "ha1b",
            "customer": create_customer().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_customerendpoint(self):
        customerendpoint = create_customerendpoint()
        url = reverse('pyfb-endpoint:pyfb_endpoint_customerendpoint_detail', args=[customerendpoint.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_customerendpoint(self):
        customerendpoint = create_customerendpoint()
        data = {
            "registration": 0,
            "password": "l$>,hg]IEzf7G)z8V5MDs>HC1",
            "description": "description",
            "name": "name1",
            "rtp_ip": "10.0.0.1/32",
            "sip_ip": "10.0.0.1/32",
            "sip_port": 5060,
            "max_calls": 10,
            "calls_per_second": 10,
            "outbound_caller_id_name": "outbound_caller_id_name",
            "outbound_caller_id_number": "outbound_caller_id_number",
            "force_caller_id": 1,
            "masq_caller_id": 1,
            "urgency_number": 1,
            "insee_code": "insee_code",
            "enabled": 1,
            "fake_ring": 1,
            "cli_debug": 1,
            "transcoding_allowed": 1,
            "recording_allowed": 1,
            "recording_always": 1,
            "recording_limit": 1,
            "recording_retention": 1,
            "sip_transport": "udp",
            "rtp_transport": "RTP/AVP",
            "rtp_tos": 184,
            "pai": 1,
            "pid": 1,
            "ha1": "ha1",
            "ha1b": "ha1b",
            "customer": customerendpoint.pk,
        }
        url = reverse('pyfb-endpoint:pyfb_endpoint_customerendpoint_update', args=[customerendpoint.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CodecViewTest(TestCase):
    '''
    Tests for Codec
    '''
    def setUp(self):
        self.client = Client()

    def test_list_codec(self):
        url = reverse('pyfb-endpoint:pyfb_endpoint_codec_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_codec(self):
        url = reverse('pyfb-endpoint:pyfb_endpoint_codec_create')
        data = {
            "name": "name",
            "number": 0,
            "ptime": 20,
            "stereo": 0,
            "rfc_name": "rfc_name",
            "description": "description",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_codec(self):
        codec = create_codec()
        url = reverse('pyfb-endpoint:pyfb_endpoint_codec_detail', args=[codec.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_codec(self):
        codec = create_codec()
        data = {
            "name": "name",
            "number": 0,
            "ptime": 20,
            "stereo": 1,
            "rfc_name": "rfc_name",
            "description": "description",
        }
        url = reverse('pyfb-endpoint:pyfb_endpoint_codec_update', args=[codec.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProviderEndpointViewTest(TestCase):
    '''
    Tests for ProviderEndpoint
    '''
    def setUp(self):
        self.client = Client()

    def test_list_providerendpoint(self):
        url = reverse('pyfb-endpoint:pyfb_endpoint_providerendpoint_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_providerendpoint(self):
        url = reverse('pyfb-endpoint:pyfb_endpoint_providerendpoint_create')
        data = {
            "name": "name",
            "max_calls": 10,
            "enabled": 1,
            "prefix": "prefix",
            "suffix": "suffix",
            "username": "username",
            "password": "password",
            "register": 1,
            "sip_proxy": "sip_proxy",
            "sip_transport": "udp",
            "sip_port": 5060,
            "realm": "realm",
            "from_domain": "from_domain",
            "expire_seconds": 10,
            "retry_seconds": 10,
            "caller_id_in_from": 1,
            "add_plus_in_caller": 0,
            "pid": 1,
            "pai": 1,
            "calls_per_second": 10,
            "rtp_transport": "RTP/AVP",
            "rtp_tos": 184,
            "provider": create_provider().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_providerendpoint(self):
        providerendpoint = create_providerendpoint()
        url = reverse('pyfb-endpoint:pyfb_endpoint_providerendpoint_detail', args=[providerendpoint.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_providerendpoint(self):
        providerendpoint = create_providerendpoint()
        data = {
            "name": "name",
            "max_calls": 10,
            "enabled": 1,
            "prefix": "prefix",
            "suffix": "suffix",
            "username": "username",
            "password": "password",
            "register": 1,
            "sip_proxy": "sip_proxy",
            "sip_transport": "udp",
            "sip_port": 5060,
            "realm": "realm",
            "from_domain": "from_domain",
            "expire_seconds": 10,
            "retry_seconds": 10,
            "caller_id_in_from": 1,
            "add_plus_in_caller": 0,
            "pid": 1,
            "pai": 1,
            "calls_per_second": 10,
            "rtp_transport": "RTP/AVP",
            "rtp_tos": 184,
            "provider": providerendpoint.pk,
        }
        url = reverse('pyfb-endpoint:pyfb_endpoint_providerendpoint_update', args=[providerendpoint.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
