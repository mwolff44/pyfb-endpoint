# Generated by Django 2.1.5 on 2019-01-30 14:27

from django.db import migrations

def initialize_data(apps, schema_editor):
    data = [
        {"model": "pyfb_endpoint.codec", "pk": 2, "fields": {"created": "2018-12-18T16:35:57.289Z", "modified": "2018-12-18T17:27:13.316Z", "name": "G.711u", "number": 0, "ptime": 20, "stereo": 0, "rfc_name": "PCMU", "description": "ITU-T G.711 PCM \u00b5-Law audio 64 kbit/s"}}, {"model": "pyfb_endpoint.codec", "pk": 3, "fields": {"created": "2018-12-18T16:36:21.657Z", "modified": "2018-12-18T17:28:49.068Z", "name": "G.729", "number": 18, "ptime": 20, "stereo": 0, "rfc_name": "G729", "description": "ITU-T G.729 and G.729a audio 8 kbit/s; Annex B is implied unless the annexb=no parameter is used"}}, {"model": "pyfb_endpoint.codec", "pk": 4, "fields": {"created": "2018-12-18T17:24:17.056Z", "modified": "2018-12-18T17:27:41.528Z", "name": "G.711a", "number": 8, "ptime": 20, "stereo": 0, "rfc_name": "PCMA", "description": "ITU-T G.711 PCM A-Law audio 64 kbit/s"}}, {"model": "pyfb_endpoint.codec", "pk": 5, "fields": {"created": "2018-12-18T17:24:52.732Z", "modified": "2018-12-18T17:45:34.900Z", "name": "GSM", "number": 3, "ptime": 20, "stereo": 0, "rfc_name": "GSM", "description": "European GSM Full Rate audio 13 kbit/s"}}, {"model": "pyfb_endpoint.codec", "pk": 7, "fields": {"created": "2018-12-18T17:30:57.733Z", "modified": "2018-12-18T17:30:57.737Z", "name": "G.723", "number": 4, "ptime": 30, "stereo": 0, "rfc_name": "G723", "description": "ITU-T G.723.1 audio"}}, {"model": "pyfb_endpoint.codec", "pk": 8, "fields": {"created": "2018-12-18T17:31:36.092Z", "modified": "2018-12-18T17:31:36.098Z", "name": "G.722", "number": 9, "ptime": 20, "stereo": 0, "rfc_name": "G722", "description": "ITU-T G.722 audio 64 kbit/s"}}
    ]

    for record in data:
        app_name, model_name = record['model'].split('.')
        ModelClass = apps.get_model(app_name, model_name)
        obj = ModelClass(**record['fields'])
        # this is required only if you have other models
        # with foreign keys referring to this object
        # obj.pk = record['pk']
        obj.save()


class Migration(migrations.Migration):

    dependencies = [
        ('pyfb_endpoint', '0005_auto_20190130_1417'),
    ]

    operations = [
        migrations.RunPython(initialize_data),
    ]
