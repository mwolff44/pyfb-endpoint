# Generated by Django 2.1.8 on 2019-07-10 14:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pyfb_kamailio', '0009_auto_20190523_1647'),
        ('pyfb_endpoint', '0012_auto_20190710_1448'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerendpoint',
            name='uacreg',
            field=models.OneToOneField(blank=True, help_text='Must be set if registration is needed', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='uac_reg', to='pyfb_kamailio.UacReg'),
        ),
        migrations.RemoveField(
            model_name='customerendpoint',
            name='codec_list',
        ),
        migrations.AddField(
            model_name='customerendpoint',
            name='codec_list',
            field=models.ManyToManyField(blank=True, help_text='if NULL, all available codecs are allowed', related_name='codecs_c', to='pyfb_endpoint.Codec'),
        ),
    ]
