# -*- coding: utf-8
from django.apps import AppConfig


class PyfbEndpointConfig(AppConfig):
    name = 'pyfb_endpoint'
